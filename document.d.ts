// Type definitions for Adobe InDesign CC Object Model
// Project: http://www.indd-skript.de/extendscriptAPI/indesign10/
// Definitions by: Magnus Kjellberg http://github.com/snidd
// Definitions: https://github.com/borisyankov/DefinitelyTyped

declare module app {
    interface Document {
        accurateLABSpots: boolean;
        accurateLABSpots: boolean;
        activeLayer: Layer;
        activeLayer: string;
        activeProcess: PreflightProcess;
        afterBlendingIntent: RenderingIntent;
        allCellStyles: CellStyle[];
        allCharacterStyles: CharacterStyle[];
        allGraphics: Graphic[]
        allObjectStyles: ObjectStyle;
        allPageItems: PageItem;
        allParagraphStyles: ParagraphStyle;
        allTableStyles: TableStyle;
        anchoredObjectDefaults:	AnchoredObjectDefault;
        anchoredObjectSettings: AnchoredObjectSetting;
        articles: Articles;
        assignments: Assignments;
        associatedXMLElement: XMLItem;
		baselineFrameGridOptions: BaselineFrameGridOption;
		bookmarks: Bookmarks;
		buttonPreferences: ButtonPreference;
        buttons: Buttons;
	    cellStyleGroups: CellStyleGroups;
		cellStyleMappings: CellStyleMappings;
		cellStyles: CellStyles;
		chapterNumberPreferences: ChapterNumberPreference;
		charStyleMappings: CharStyleMappings;
		characterStyleGroups: CharacterStyleGroups;
		characterStyles: CharacterStyles;
		checkBoxes: CheckBoxes;
		cjkGridPreferences: CjkGridPreference;
		cmykPolicy: ColorSettingsPolicy;
		cmykProfile: string;
        cmykProfileList: string;
		colorGroups: ColorGroups;
		colors: Colors;
		comboBoxes: ComboBoxes;
		compositeFonts: CompositeFonts;
		conditionSets: ConditionSets;
		conditionalTextPreferences: ConditionalTextPreference;
		conditions: Conditions;
		converted: Boolean;
		crossReferenceFormats: CrossReferenceFormats;
		crossReferenceSources: CrossReferenceSources;
		dashedStrokeStyles: DashedStrokeStyles;
		dataMergeImagePlaceholders: DataMergeImagePlaceholders;
		dataMergeOptions: DataMergeOption;
		dataMergeProperties: DataMerge;
		dataMergeQrcodePlaceholders: DataMergeQrcodePlaceholders;
		dataMergeTextPlaceholders: DataMergeTextPlaceholders;
		defaultImageIntent: RenderingIntent;
		dictionaryPreferences: DictionaryPreference;
		documentPreferences: DocumentPreference;
		dottedStrokeStyles: DottedStrokeStyles;
		dtds: DTDs;
		editingState: EditingState;
		epstexts: EPSTexts;
		epubExportPreferences: EPubExportPreference;
		epubFixedLayoutExportPreferences: EPubFixedLayoutExportPreference;
		eventListeners: EventListeners;
		events: Events;
		exportForWebPreferences: ExportForWebPreference;
		filePath: File;
		fonts: Fonts;
		footnoteOptions: FootnoteOption;
		formFields: FormFields;
		frameFittingOptions: FrameFittingOption;
		fullName: File;
		galleyPreferences: GalleyPreference;
		gradients: Gradients;
		graphicLines: GraphicLines;
		gridPreferences: GridPreference;
		groups: Groups;
		guidePreferences: GuidePreference;
		guides: Guides;
		htmlExportPreferences: HTMLExportPreference;
		hyperlinkExternalPageDestinations: HyperlinkExternalPageDestinations;
		hyperlinkPageDestinations: HyperlinkPageDestinations;
		hyperlinkPageItemSources: HyperlinkPageItemSources;
		hyperlinkTextDestinations: HyperlinkTextDestinations;
		hyperlinkTextSources: HyperlinkTextSources;
		hyperlinkURLDestinations: HyperlinkURLDestinations;
		hyperlinks: Hyperlinks;
		hyphenationExceptions: HyphenationExceptions;
		id: number;
		index: number;
		indexGenerationOptions: IndexOptions;
		indexes: Indexes;
		indexingSortOptions: IndexingSortOptions;
		inks: Inks;
		isValid: Boolean;
		kinsokuTables: KinsokuTables;
		label: string;
		languages: Languages;
		layers: Layers;
		layoutAdjustmentPreferences: LayoutAdjustmentPreference;
		layoutGridData: LayoutGridDataInformation;
		layoutWindows: LayoutWindows;
		linkedPageItemOptions: LinkedPageItemOption;
		linkedStoryOptions: LinkedStoryOption;
		links: Links;
		listBoxes: ListBoxes;
		marginPreferences: MarginPreference;
		masterSpreads: MasterSpreads;
		metadataPreferences: MetadataPreference;
		mixedInkGroups: MixedInkGroups;
		mixedInks: MixedInks;
		modified: Boolean;
		mojikumiTables: MojikumiTables;
		mojikumiUIPreferences: MojikumiUiPreference;
		motionPresets: MotionPresets;
		multiStateObjects: MultiStateObjects;
		name: string;
		namedGrids: NamedGrids;
		numberingLists: NumberingLists;
		objectStyleGroups: ObjectStyleGroups;
		objectStyles: ObjectStyles;
		ovals: Ovals;
		pageItemDefaults: PageItemDefault;
		pageItems: PageItems;
		pages: Pages;
		paraStyleMappings: ParaStyleMappings;
		paragraphDestinations: ParagraphDestinations;
		paragraphStyleGroups: ParagraphStyleGroups;
		paragraphStyles: ParagraphStyles;
		parent: Application;
		pasteboardPreferences: PasteboardPreference;
		placeGuns: PlaceGun;
		polygons: Polygons;
		preferences: Preferences;
		preflightOptions: PreflightOption;
		preflightProfiles: PreflightProfiles;
		printBookletOptions: PrintBookletOption;
		printBookletPrintPreferences: PrintBookletPrintPreference;
		printPreferences: PrintPreference;
		properties: Object;
		radioButtons: RadioButtons;
		readOnly: Boolean;
		recovered: Boolean;
		rectangles: Rectangles;
        redoHistory: string;
		redoName: string;
        rgbPolicy: ColorSettingsPolicy;
		rgbProfile: string;
        rgbProfileList: string;
		saved: Boolean;
		sections: Sections;
        selection	: any;
        selection: NothingEnum.NOTHING;
        selectionKeyObject: PageItem;
        selectionKeyObject: NothingEnum.NOTHING;
		signatureFields: SignatureFields;
        solidColorIntent: RenderingIntent;
		splineItems: SplineItems;
		spreads: Spreads;
		stories: Stories;
		storyGridData: StoryGridDataInformation;
		storyPreferences: StoryPreference;
		storyWindows: StoryWindows;
		stripedStrokeStyles: StripedStrokeStyles;
		strokeStyles: StrokeStyles;
		swatches: Swatches;
		tableStyleGroups: TableStyleGroups;
		tableStyleMappings: TableStyleMappings;
		tableStyles: TableStyles;
		taggedPDFPreferences: TaggedPDFPreference;
		textBoxes: TextBoxes;
		textDefaults: TextDefault;
		textFramePreferences: TextFramePreference;
		textFrames: TextFrames;
		textPreferences: TextPreference;
		textVariables: TextVariables;
		textWrapPreferences: TextWrapPreference;
		tints: Tints;
		tocStyles: TOCStyles;
		transparencyPreferences: TransparencyPreference;
		trapPresets: TrapPresets;
        undoHistory: string;
		undoName: string;
        unusedSwatches: Swatch;
		validationErrors: ValidationErrors;
        versionState: VersionState;
		viewPreferences: ViewPreference;
		visible: Boolean;
		watermarkPreferences: WatermarkPreference;
		windows: Windows;
		xmlComments: XMLComments;
		xmlElements: XMLElements;
		xmlExportMaps: XMLExportMaps;
		xmlExportPreferences: XMLExportPreference;
		xmlImportMaps: XMLImportMaps;
		xmlImportPreferences: XMLImportPreference;
		xmlInstructions: XMLInstructions;
		xmlItems: XMLItems;
		xmlPreferences: XMLPreference;
		xmlStories: XmlStories;
		xmlTags: XMLTags;
		xmlViewPreferences: XMLViewPreference;
        zeroPoint: number[];
        zeroPoint: string[];

        /**
         * Adds an event listener.
         */
        addEventListener(eventType:string, handler:File, captures?:boolean):EventListener;
        addEventListener(eventType:string, handler:Function, captures?:boolean):EventListener;

        /**
         * Align page items.
         */
        align(alignDistributeItems:PageItem[], alignOption:AlignOptions, alignDistributeBounds:AlignDistributeBounds, reference:PageItem):void;

        /**
         * asynchronously exports the object(s) to a file.
         */
        asynchronousExportFile(format:ExportFormat, to:File, showingOptions?:Boolean,
                               using?:PDFExportPreset, versionComments?:String, forceSave?:Boolean):BackgroundTask;
        asynchronousExportFile(format:string, to:File, showingOptions?:Boolean,
                               using?:PDFExportPreset, versionComments?:String, forceSave?:Boolean):BackgroundTask;
        /**
         * Change comoser to optyca
         */
        changeComposer();

        /**
         * Finds glyphs that match the find what value and replaces the glyphs with the change to value.
         * @param reverseOrder If true, returns the results in reverse order. (Optional)
         */
        changeGlyph(reverseOrder?:Boolean):Text;

        /**
         * Finds text that matches the find what value and replaces the text with the change to value.
         * @param reverseOrder If true, returns the results in reverse order. (Optional)
         */
        changeGrep(reverseOrder?:Boolean):Text;

        /**
         * Finds objects that match the find what value and replace the objects with the change to value.
         * @param reverseOrder If true, returns the results in reverse order. (Optional)
         */
        changeObject(reverseOrder?:Boolean):PageItem;

        /**
         * Finds text that matches the find character type value and replaces the text with the change character type value.
         * @param reverseOrder If true, returns the results in reverse order. (Optional)
         */
        changeTransliterate(reverseOrder?:Boolean):Text

        /**
         * Check in to Version Cue.
         * @param versionComments The comment for this version (Optional)
         * @param forceSave Forcibly save a version (Optional)
         */
        checkIn(versionComments:string, forceSave:boolean);


    }
}